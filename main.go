package main

import (
	"os"

	"gitlab.com/danscott/yagbe-go/pkg/cpu"
)

func main() {
	romFile := os.Args[1]

	c := cpu.New()

	c.Run(romFile)
}
