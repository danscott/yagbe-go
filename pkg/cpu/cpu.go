package cpu

import (
	"fmt"
	"io/ioutil"
	"log"

	"gitlab.com/danscott/yagbe-go/pkg/memory"
	"gitlab.com/danscott/yagbe-go/pkg/registers"
)

type ticks struct {
	t uint
	m uint
}

type interrupts struct {
	master bool
	enable bool
}

// CPU runs the show
type CPU struct {
	mem       memory.ReadWriter
	reg       registers.ReadWriter
	clock     ticks
	interrupt interrupts
}

type op struct {
	name string
	run  func(c *CPU) (ticks, error)
}

var undefOp = op{
	name: "UNDEFINED",
	run: func(c *CPU) (ticks, error) {
		return ticks{}, fmt.Errorf("No such operation")
	},
}

var opMap = make(map[byte]op)

func regOp(code byte, name string, run func(c *CPU) (ticks, error)) {
	opMap[code] = op{name, run}
}

var charCodes = []byte{
	' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
	' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
	' ', '!', '"', '#', '$', '%', '&', '\'', '(', ')', '*', '+', ',', '-', '.', '/',
	'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ':', ';', '<', '=', '>', '?',
	'@', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O',
	'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '[', '¥', ']', '^', '_',
}

// New returns a new cpu instance
func New() *CPU {
	c := CPU{
		mem: memory.New(),
		reg: registers.New(),
	}

	return &c
}

// Reset resets all system values
func (c *CPU) Reset() {
	c.mem.Reset()
	c.reg.Reset()
	c.interrupt.enable = false
	c.interrupt.master = true
}

// Run runs things
func (c *CPU) Run(romFile string) {
	log.SetFlags(0)
	c.loadRom(romFile)
	for {
		if err := c.runOp(); err != nil {
			panic(fmt.Sprintf("%v", err))
		}
	}
}

func (c *CPU) loadRom(romFile string) error {
	data, err := ioutil.ReadFile(romFile)
	if err != nil {
		return fmt.Errorf("Error reading rom file %s: %v", romFile, err)
	}

	for i, b := range data {
		c.mem.WriteByteTo(b, uint16(i))
	}

	return nil
}

func (c *CPU) runOp() error {
	log.SetPrefix(fmt.Sprintf("%5v [%#04x] ", c.clock.t, c.reg.PC()))
	op, err := c.readNextByte()
	if err != nil {
		return err
	}

	handler, ok := opMap[op]
	if !ok {
		return fmt.Errorf("Handler not implemented for operation %#02x", op)
	}

	tickCount, err := handler.run(c)
	if err != nil {
		return err
	}

	c.clock.m += tickCount.m
	c.clock.t += tickCount.t

	return nil
}

func (c *CPU) readNextByte() (byte, error) {
	val, err := c.mem.ReadByteAt(c.reg.PC())
	if err != nil {
		return 0, fmt.Errorf("Error reading next byte: %v", err)
	}
	c.reg.IncPCBy(1)

	return val, nil
}

func (c *CPU) readNextShort() (uint16, error) {
	val, err := c.mem.ReadShortAt(c.reg.PC())
	if err != nil {
		return 0, fmt.Errorf("Error reading next short: %v", err)
	}
	c.reg.IncPCBy(2)

	return val, nil
}

func (c *CPU) pushShort(val uint16) {
	c.reg.DecSPBy(2)
	c.mem.WriteShortTo(val, c.reg.SP())
}

func (c *CPU) popShort() (uint16, error) {
	val, err := c.mem.ReadShortAt(c.reg.SP())
	if err != nil {
		return 0, fmt.Errorf("Error popping value from stack: %v", err)
	}
	c.reg.IncSPBy(2)
	return val, nil
}
