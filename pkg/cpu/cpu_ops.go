package cpu

import (
	"fmt"
	"log"
	"strings"
)

var errTicks = ticks{0, 0}

func tick(t uint) (ticks, error) {
	return ticks{t: t, m: t * 4}, nil
}

func logOp(name string, vals ...interface{}) {
	sVals := make([]string, len(vals), len(vals))
	for i, v := range vals {
		sVals[i] = fmt.Sprintf("%#04x", v)
	}
	log.Printf("%-15s: %v", name, strings.Join(sVals, ", "))
}

func init() {
	regOp(0x00, "NOP", nop)
	regOp(0x05, "DEC B", decB)
	regOp(0x06, "LD B,n", ldBn)
	regOp(0x0e, "LD C,n", ldCn)

	regOp(0x18, "JR n", jrN)

	regOp(0x20, "JR NZ,n", jrnzn)
	regOp(0x21, "LD HL,nn", ldHLnn)

	regOp(0x31, "LD SP,nn", ldSPnn)
	regOp(0x32, "LD (HLD),A", ldHLDA)
	regOp(0x3e, "LD A,n", ldAn)

	regOp(0xaf, "XOR A", xorA)

	regOp(0xc3, "JP nn", jpAnn)
	regOp(0xcd, "CALL nn", callnn)

	regOp(0xe0, "LDH (n),A", ldhnA)
	regOp(0xea, "LD (nn),A", ldnnA)

	regOp(0xf1, "POP AF", popAF)
	regOp(0xf3, "DI", di)
	regOp(0xf5, "PUSH AF", pushAF)
	regOp(0xfc, "")
}

// 0x00
func nop(c *CPU) (ticks, error) {
	logOp("nop")
	return tick(2)
}

// 0x05
func decB(c *CPU) (ticks, error) {
	b := c.reg.B()
	c.reg.SetFlagH((b & 0x0f) == 0x00)
	next := b - 1
	logOp("dec B", b, next)
	c.reg.SetFlagZ(next == 0)
	c.reg.SetFlagN(true)
	c.reg.SetB(next)
	return tick(1)
}

// 0x06
func ldBn(c *CPU) (ticks, error) {
	n, err := c.readNextByte()
	if err != nil {
		return errTicks, err
	}
	logOp("ld b,n", n)
	c.reg.SetB(n)
	return tick(2)
}

// 0x0e
func ldCn(c *CPU) (ticks, error) {
	n, err := c.readNextByte()
	if err != nil {
		return errTicks, err
	}
	logOp("ld c,n", n)
	c.reg.SetC(n)
	return tick(2)
}

func jr(c *CPU, n byte) (ticks, error) {
	pc := int32(c.reg.PC())
	jr := int8(n)
	next := pc + int32(jr)
	c.reg.SetPC(uint16(next))
	return tick(3)
}

// 0x18
func jrN(c *CPU) (ticks, error) {
	n, err := c.readNextByte()
	if err != nil {
		return errTicks, err
	}
	logOp("jr n", n, int8(n))
	return jr(c, n)
}

// 0x20
func jrnzn(c *CPU) (ticks, error) {
	if c.reg.FlagZ() {
		return tick(2)
	}
	return jrN(c)
}

// 0x21
func ldHLnn(c *CPU) (ticks, error) {
	nn, err := c.readNextShort()
	if err != nil {
		return errTicks, err
	}
	logOp("ld hl,nn", nn)
	c.reg.SetHL(nn)
	return tick(3)
}

// 0x31
func ldSPnn(c *CPU) (ticks, error) {
	nn, err := c.readNextShort()
	if err != nil {
		return errTicks, err
	}
	logOp("ld sp,nn", nn)
	c.reg.SetSP(nn)
	return tick(4)
}

// 0x32
func ldHLDA(c *CPU) (ticks, error) {
	logOp("ld (hld),a", c.reg.HL(), c.reg.A())
	if err := c.mem.WriteByteTo(c.reg.A(), c.reg.HL()); err != nil {
		return errTicks, err
	}
	c.reg.SetHL(c.reg.HL() - 1)
	return tick(2)
}

// 0x3e
func ldAn(c *CPU) (ticks, error) {
	n, err := c.readNextByte()
	if err != nil {
		return errTicks, err
	}
	logOp("ld a,n", n)
	c.reg.SetA(n)
	return tick(2)
}

// 0xc3
func jpAnn(c *CPU) (ticks, error) {
	nn, err := c.readNextShort()
	if err != nil {
		return errTicks, err
	}
	logOp("jp nn", nn)
	c.reg.SetPC(nn)
	return tick(4)
}

// 0xaf
func xorA(c *CPU) (ticks, error) {
	logOp("xor a", c.reg.A())
	xor(c.reg.A(), c)
	return tick(1)
}

func xor(val byte, c *CPU) {
	c.reg.SetA(c.reg.A() ^ val)
	c.reg.SetFlagZ(c.reg.A() == 0)
	c.reg.SetFlagCY(false)
	c.reg.SetFlagH(false)
	c.reg.SetFlagN(false)
}

// 0xcd
func callnn(c *CPU) (ticks, error) {
	c.pushShort(c.reg.PC())
	nn, err := c.readNextShort()
	if err != nil {
		return errTicks, err
	}
	logOp("call nn", nn)
	c.reg.SetSP(nn)
	return tick(6)
}

// 0xe0
func ldhnA(c *CPU) (ticks, error) {
	n, err := c.readNextByte()
	if err != nil {
		return errTicks, err
	}
	tgt := uint16(n) | 0xFF00
	logOp("ld (n),a", n, c.reg.A())
	c.mem.WriteByteTo(c.reg.A(), tgt)
	return tick(3)
}

// 0xea
func ldnnA(c *CPU) (ticks, error) {
	nn, err := c.readNextShort()
	if err != nil {
		return errTicks, err
	}
	logOp("ld (nn),a", nn, c.reg.A())
	c.mem.WriteByteTo(c.reg.A(), nn)
	return tick(4)
}

// 0xf1
func popAF(c *CPU) (ticks, error) {
	val, err := c.popShort()
	if err != nil {
		return errTicks, err
	}
	logOp("pop af", val)
	c.reg.SetAF(val)
	return tick(3)
}

// 0xf3
func di(c *CPU) (ticks, error) {
	logOp("di", "")
	c.interrupt.master = false
	return tick(1)
}

// 0xf5
func pushAF(c *CPU) (ticks, error) {
	logOp("push af", c.reg.AF())
	c.pushShort(c.reg.AF())
	return tick(4)
}
