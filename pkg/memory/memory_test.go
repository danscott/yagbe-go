package memory_test

import (
	"testing"

	"gitlab.com/danscott/yagbe-go/pkg/memory"
)

func TestByteMemoryAccess(t *testing.T) {
	m := memory.New().(*memory.Memory)
	cases := []struct {
		bankName string
		bank     []byte
		startAdr uint16
	}{
		{"Cart", m.Cart[:], memory.CartStart},
		{"Vram", m.Vram[:], memory.VramStart},
		{"Sram", m.Sram[:], memory.SramStart},
		{"Wram", m.Wram[:], memory.WramStart},
		{"Echo", m.Echo[:], memory.EchoStart},
		{"Oam", m.Oam[:], memory.OamStart},
		{"Io", m.Io[:], memory.IoStart},
		{"Hram", m.Hram[:], memory.HramStart},
	}

	for _, c := range cases {
		t.Run(c.bankName, func(t *testing.T) {
			adr := uint16(0x0030) + c.startAdr
			err := m.WriteByteTo(0x0012, adr)
			if err != nil {
				t.Errorf("Expected to write 0x0012 to address %#x but encountered error: %v", adr, err)
			}
			if c.bank[0x0030] != 0x0012 {
				t.Errorf("Expected Cart address %#x to be 0x0012 but found %#x", adr, c.bank[0x0030])
			}

			val, err := m.ReadByteAt(adr)
			if err != nil {
				t.Errorf("Expected to read 0x0012 from address %#x but encountered error: %v", adr, err)
			}
			if val != 0x0012 {
				t.Errorf("Expected to read 0x0012 from address %#x but got %#x", adr, val)
			}
		})
	}
}

func TestShortMemoryAccess(t *testing.T) {
	m := memory.New().(*memory.Memory)
	cases := []struct {
		bankName string
		bank     []byte
		startAdr uint16
	}{
		{"Cart", m.Cart[:], memory.CartStart},
		{"Vram", m.Vram[:], memory.VramStart},
		{"Sram", m.Sram[:], memory.SramStart},
		{"Wram", m.Wram[:], memory.WramStart},
		{"Echo", m.Echo[:], memory.EchoStart},
		{"Oam", m.Oam[:], memory.OamStart},
		{"Io", m.Io[:], memory.IoStart},
		{"Hram", m.Hram[:], memory.HramStart},
	}

	for _, c := range cases {
		t.Run(c.bankName, func(t *testing.T) {
			adr := uint16(0x0030) + c.startAdr
			err := m.WriteShortTo(0xA341, adr)
			if err != nil {
				t.Errorf("Expected to write 0xA341 to address %#x but encountered error: %v", adr, err)
			}
			if c.bank[0x0030] != uint8(0xA341&0x00FF) {
				t.Errorf("Expected Cart address %#x to be %#x but found %#x", adr, 0xA341&0x00FF, c.bank[0x0030])
			}
			if c.bank[0x0031] != uint8(0xA341>>8) {
				t.Errorf("Expected Cart address %#x to be %#x but found %#x", adr, 0xA341>>8, c.bank[0x0031])
			}

			val, err := m.ReadShortAt(adr)
			if err != nil {
				t.Errorf("Expected to read 0xA341 from address %#x but encountered error: %v", adr, err)
			}
			if val != 0xA341 {
				t.Errorf("Expected to read 0xA341 from address %#x but got %#x", adr, val)
			}
		})
	}
}

func TestWriteByteToWram(t *testing.T) {
	m := memory.New().(*memory.Memory)
	val := byte(0x12)
	adr := uint16(memory.WramStart + 0x003)

	err := m.WriteByteTo(val, adr)

	if err != nil {
		t.Errorf("Expected to write %#x to address %#x but encountered error: %v", val, adr, err)
	}

	if m.Wram[0x003] != val {
		t.Errorf("Expected Wram address %#x to be %#x but found %#x", adr, val, m.Wram[0x003])
	}

	if m.Echo[0x003] != val {
		t.Errorf("Expected write to Wram address %#x to be duplicated in Echo, but found %#x", adr, m.Echo[0x003])
	}
}

func TestWriteByteToEcho(t *testing.T) {
	m := memory.New().(*memory.Memory)
	val := byte(0x12)
	adr := uint16(memory.EchoStart + 0x003)

	err := m.WriteByteTo(val, adr)

	if err != nil {
		t.Errorf("Expected to write %#x to address %#x but encountered error: %v", val, adr, err)
	}

	if m.Echo[0x003] != val {
		t.Errorf("Expected Echo address %#x to be %#x but found %#x", adr, val, m.Echo[0x003])
	}

	if m.Wram[0x003] != val {
		t.Errorf("Expected write to Echo address %#x to be duplicated in Echo, but found %#x", adr, m.Wram[0x003])
	}
}
