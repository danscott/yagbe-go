package memory

import "fmt"

const (
	cartSize = 0x8000
	vramSize = 0x2000
	wramSize = 0x2000
	sramSize = 0x2000
	oamSize  = 0x100
	ioSize   = 0x100
	hramSize = 0x80
)

// Constants denoting the start addresses of the various ram types
const (
	CartStart = 0x0000
	VramStart = 0x8000
	SramStart = 0xA000
	WramStart = 0xC000
	EchoStart = 0xE000
	OamStart  = 0xFE00
	IoStart   = 0xFF00
	HramStart = 0xFF80
	IerStart  = 0xFFFF
)

const (
	ioPre  = 0xFEA0
	ioPost = 0xFF4C
)

var ioReset = [0x100]byte{
	0x0F, 0x00, 0x7C, 0xFF, 0x00, 0x00, 0x00, 0xF8, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x01,
	0x80, 0xBF, 0xF3, 0xFF, 0xBF, 0xFF, 0x3F, 0x00, 0xFF, 0xBF, 0x7F, 0xFF, 0x9F, 0xFF, 0xBF, 0xFF,
	0xFF, 0x00, 0x00, 0xBF, 0x77, 0xF3, 0xF1, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
	0x00, 0xFF, 0x00, 0xFF, 0x00, 0xFF, 0x00, 0xFF, 0x00, 0xFF, 0x00, 0xFF, 0x00, 0xFF, 0x00, 0xFF,
	0x91, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFC, 0x00, 0x00, 0x00, 0x00, 0xFF, 0x7E, 0xFF, 0xFE,
	0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x3E, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
	0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xC0, 0xFF, 0xC1, 0x00, 0xFE, 0xFF, 0xFF, 0xFF,
	0xF8, 0xFF, 0x00, 0x00, 0x00, 0x8F, 0x00, 0x00, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
	0xCE, 0xED, 0x66, 0x66, 0xCC, 0x0D, 0x00, 0x0B, 0x03, 0x73, 0x00, 0x83, 0x00, 0x0C, 0x00, 0x0D,
	0x00, 0x08, 0x11, 0x1F, 0x88, 0x89, 0x00, 0x0E, 0xDC, 0xCC, 0x6E, 0xE6, 0xDD, 0xDD, 0xD9, 0x99,
	0xBB, 0xBB, 0x67, 0x63, 0x6E, 0x0E, 0xEC, 0xCC, 0xDD, 0xDC, 0x99, 0x9F, 0xBB, 0xB9, 0x33, 0x3E,
	0x45, 0xEC, 0x52, 0xFA, 0x08, 0xB7, 0x07, 0x5D, 0x01, 0xFD, 0xC0, 0xFF, 0x08, 0xFC, 0x00, 0xE5,
	0x0B, 0xF8, 0xC2, 0xCE, 0xF4, 0xF9, 0x0F, 0x7F, 0x45, 0x6D, 0x3D, 0xFE, 0x46, 0x97, 0x33, 0x5E,
	0x08, 0xEF, 0xF1, 0xFF, 0x86, 0x83, 0x24, 0x74, 0x12, 0xFC, 0x00, 0x9F, 0xB4, 0xB7, 0x06, 0xD5,
	0xD0, 0x7A, 0x00, 0x9E, 0x04, 0x5F, 0x41, 0x2F, 0x1D, 0x77, 0x36, 0x75, 0x81, 0xAA, 0x70, 0x3A,
	0x98, 0xD1, 0x71, 0x02, 0x4D, 0x01, 0xC1, 0xFF, 0x0D, 0x00, 0xD3, 0x05, 0xF9, 0x00, 0x0B, 0x00,
}

// ByteWriter allows writing a single byte to a memory address
type ByteWriter interface {
	WriteByteTo(val byte, adr uint16) error
}

// ByteReader allows reading a single byte at a memory address
type ByteReader interface {
	ReadByteAt(adr uint16) (byte, error)
}

// ShortWriter allows writing two bytes starting at a memory address
type ShortWriter interface {
	WriteShortTo(val, adr uint16) error
}

// ShortReader allows reading two bytes starting at a memory address
type ShortReader interface {
	ReadShortAt(adr uint16) (uint16, error)
}

// ReadWriter allows reading from and writing to memory
type ReadWriter interface {
	ByteReader
	ByteWriter
	ShortReader
	ShortWriter
	Reset()
}

// Memory is the default memory ReadWriter implementation
type Memory struct {
	Cart []byte
	Vram []byte
	Sram []byte
	Wram []byte
	Echo []byte
	Oam  []byte
	Io   []byte
	Hram []byte
}

// New returns a pointer to a new Memory instance
func New() ReadWriter {
	m := Memory{Cart: make([]byte, cartSize)}
	m.Reset()
	return &m
}

// Reset resets a memory's content (excluding Cart) to initial state
func (m *Memory) Reset() {
	m.Vram = make([]byte, vramSize)
	m.Sram = make([]byte, sramSize)
	m.Wram = make([]byte, wramSize)
	m.Echo = m.Wram[:]
	m.Oam = make([]byte, oamSize)
	m.Io = make([]byte, ioSize)
	m.Hram = make([]byte, hramSize)

	for i, v := range ioReset {
		m.Io[i] = v
	}
}

// WriteByteTo writes a single byte to memory at the specified address
func (m *Memory) WriteByteTo(val byte, adr uint16) error {
	wc := 0
	wc += writeIfIn(&m.Cart, CartStart, VramStart, adr, val)
	wc += writeIfIn(&m.Vram, VramStart, SramStart, adr, val)
	wc += writeIfIn(&m.Sram, SramStart, WramStart, adr, val)
	wc += writeIfIn(&m.Wram, WramStart, EchoStart, adr, val)
	wc += writeIfIn(&m.Echo, EchoStart, OamStart, adr, val)
	wc += writeIfIn(&m.Oam, OamStart, ioPre, adr, val)
	wc += writeIfIn(&m.Io, IoStart, ioPost, adr, val)
	wc += writeIfIn(&m.Hram, HramStart, IerStart, adr, val)

	if wc == 0 {
		return fmt.Errorf("Attempt to write value %#x to invalid memory location: %#x", val, adr)
	}

	return nil
}

func writeIfIn(arr *[]byte, startAdr, endAdr, writeAdr uint16, val byte) int {
	if startAdr <= writeAdr && writeAdr < endAdr {
		(*arr)[writeAdr-startAdr] = val
		return 1
	}

	return 0
}

// ReadByteAt returns the byte stored at the specified address
func (m *Memory) ReadByteAt(adr uint16) (byte, error) {
	if val, ok := readIfIn(&m.Cart, CartStart, VramStart, adr); ok {
		return val, nil
	}
	if val, ok := readIfIn(&m.Vram, VramStart, SramStart, adr); ok {
		return val, nil
	}
	if val, ok := readIfIn(&m.Sram, SramStart, WramStart, adr); ok {
		return val, nil
	}
	if val, ok := readIfIn(&m.Wram, WramStart, EchoStart, adr); ok {
		return val, nil
	}
	if val, ok := readIfIn(&m.Echo, EchoStart, OamStart, adr); ok {
		return val, nil
	}
	if val, ok := readIfIn(&m.Oam, OamStart, ioPre, adr); ok {
		return val, nil
	}
	if val, ok := readIfIn(&m.Io, IoStart, ioPost, adr); ok {
		return val, nil
	}
	if val, ok := readIfIn(&m.Hram, HramStart, IerStart, adr); ok {
		return val, nil
	}
	return 0, fmt.Errorf("Attempted to read invalid memory address %#x", adr)
}

func readIfIn(arr *[]byte, startAdr, endAdr, readAdr uint16) (byte, bool) {
	if startAdr <= readAdr && readAdr < endAdr {
		return (*arr)[readAdr-startAdr], true
	}
	return 0, false
}

// WriteShortTo writes two bytes a uint16 to memory starting at the specified address
func (m *Memory) WriteShortTo(val, adr uint16) error {
	if err := m.WriteByteTo(byte(val&0x00FF), adr); err != nil {
		return fmt.Errorf("Failed to write %#x to %#x: %v", val, adr, err)
	}
	if err := m.WriteByteTo(byte(val>>8), adr+1); err != nil {
		return fmt.Errorf("Failed to write %#x to %#x: %v", val, adr, err)
	}
	return nil
}

// ReadShortAt reads two bytes as a uint16 from memory starting at the specified address
func (m *Memory) ReadShortAt(adr uint16) (uint16, error) {
	r, err := m.ReadByteAt(adr)
	if err != nil {
		return 0, fmt.Errorf("Failed to read short at %#x: %v", adr, err)
	}
	l, err := m.ReadByteAt(adr + 1)
	if err != nil {
		return 0, fmt.Errorf("Failed to read short at %#x: %v", adr, err)
	}
	return uint16(l)<<8 | uint16(r), nil
}
