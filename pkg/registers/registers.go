package registers

// RegReader reads values of registers
type RegReader interface {
	A() byte
	F() byte
	B() byte
	C() byte
	D() byte
	E() byte
	H() byte
	L() byte

	AF() uint16
	BC() uint16
	DE() uint16
	HL() uint16

	SP() uint16
	PC() uint16
}

// RegWriter updates register values
type RegWriter interface {
	SetA(val byte)
	SetF(val byte)
	SetB(val byte)
	SetC(val byte)
	SetD(val byte)
	SetE(val byte)
	SetH(val byte)
	SetL(val byte)

	SetAF(val uint16)
	SetBC(val uint16)
	SetDE(val uint16)
	SetHL(val uint16)

	SetPC(val uint16)
	SetSP(val uint16)

	IncPCBy(val uint16)
	IncSPBy(val uint16)

	DecPCBy(val uint16)
	DecSPBy(val uint16)
}

// FlagReader returns the true/false state of a flag
type FlagReader interface {
	FlagZ() bool
	FlagN() bool
	FlagH() bool
	FlagCY() bool
}

// FlagWriter sets or clears flags
type FlagWriter interface {
	SetFlagZ(val bool)
	SetFlagN(val bool)
	SetFlagH(val bool)
	SetFlagCY(val bool)
}

// ReadWriter provides access to flags and registers
type ReadWriter interface {
	RegReader
	RegWriter
	FlagReader
	FlagWriter
	Reset()
}

// BytePair holds the two 8 bit components of a register pair
type BytePair struct {
	top    byte
	bottom byte
}

// SetTop sets the first 8bit component
func (b *BytePair) SetTop(val byte) {
	b.top = val
}

// Top returns the value of the 8 bits
func (b *BytePair) Top() byte {
	return b.top
}

// SetBottom sets the second 8bit component
func (b *BytePair) SetBottom(val byte) {
	b.bottom = val
}

// Bottom returns the value of the second 8bit component
func (b *BytePair) Bottom() byte {
	return b.bottom
}

// SetValue sets the combined 16bit value
func (b *BytePair) SetValue(val uint16) {
	b.top = byte(val >> 8)
	b.bottom = byte(val & 0xFF)
}

// Value returns the combined uint16 value of the top and bottom 8bit components
func (b *BytePair) Value() uint16 {
	return uint16(b.top)<<8 | uint16(b.bottom)
}

type registers struct {
	af BytePair
	bc BytePair
	de BytePair
	hl BytePair
	sp uint16
	pc uint16
}

// New returns a new instance of Registers
func New() ReadWriter {
	r := registers{}
	r.Reset()
	return &r
}

func (r *registers) Reset() {
	r.SetA(0x01)
	r.SetF(0xB0)
	r.SetB(0x00)
	r.SetC(0x13)
	r.SetD(0x00)
	r.SetE(0xD8)
	r.SetH(0x01)
	r.SetL(0x4d)
	r.sp = 0xFFFE
	r.pc = 0x0100
}

// A returns the value of the a register
func (r *registers) A() byte { return r.af.Top() }

// F returns the value of the f register
func (r *registers) F() byte { return r.af.Bottom() }

// B returns the value of the b register
func (r *registers) B() byte { return r.bc.Top() }

// C returns the value of the c register
func (r *registers) C() byte { return r.bc.Bottom() }

// D returns the value of the d register
func (r *registers) D() byte { return r.de.Top() }

// E returns the value of the e register
func (r *registers) E() byte { return r.de.Bottom() }

// H returns the value of the h register
func (r *registers) H() byte { return r.hl.Top() }

// L returns the value of the l register
func (r *registers) L() byte { return r.hl.Bottom() }

// SetA sets the value of the A register
func (r *registers) SetA(val byte) { r.af.SetTop(val) }

// SetF sets the value of the F register
func (r *registers) SetF(val byte) { r.af.SetBottom(val) }

// SetB sets the value of the B register
func (r *registers) SetB(val byte) { r.bc.SetTop(val) }

// SetC sets the value of the C register
func (r *registers) SetC(val byte) { r.bc.SetBottom(val) }

// SetD sets the value of the D register
func (r *registers) SetD(val byte) { r.de.SetTop(val) }

// SetE sets the value of the E register
func (r *registers) SetE(val byte) { r.de.SetBottom(val) }

// SetH sets the value of the H register
func (r *registers) SetH(val byte) { r.hl.SetTop(val) }

// SetL sets the value of the L register
func (r *registers) SetL(val byte) { r.hl.SetBottom(val) }

// AF returns the combined value of the A and F registers
func (r *registers) AF() uint16 { return r.af.Value() }

// BC returns the combined value of the B and C registers
func (r *registers) BC() uint16 { return r.bc.Value() }

// DE returns the combined value of the D and E registers
func (r *registers) DE() uint16 { return r.de.Value() }

// HL returns the combined value of the H and L registers
func (r *registers) HL() uint16 { return r.hl.Value() }

// SP returns the combined value of the H and L registers
func (r *registers) SP() uint16 { return r.sp }

// PC returns the combined value of the H and L registers
func (r *registers) PC() uint16 { return r.pc }

// SetAF sets the A and F registers to the top and bottom 8 bits of val respectively
func (r *registers) SetAF(val uint16) { r.af.SetValue(val) }

// SetBC sets the B and C registers to the top and bottom 8 bits of val respectively
func (r *registers) SetBC(val uint16) { r.bc.SetValue(val) }

// SetDE sets the D and E registers to the top and bottom 8 bits of val respectively
func (r *registers) SetDE(val uint16) { r.de.SetValue(val) }

// SetHL sets the H and L registers to the top and bottom 8 bits of val respectively
func (r *registers) SetHL(val uint16) { r.hl.SetValue(val) }

// SetSP assigns the stack pointer a new address to point to
func (r *registers) SetSP(val uint16) { r.sp = val }

// SetPC assigns the program counter a new address to point to
func (r *registers) SetPC(val uint16) { r.pc = val }

// IncSPBy increments the stack pointer by the specified amount
func (r *registers) IncSPBy(val uint16) { r.sp += val }

// IncPCBy increments the program counter by the specified amount
func (r *registers) IncPCBy(val uint16) { r.pc += val }

// DecSPBy decrements the stack pointer by the specified amount
func (r *registers) DecSPBy(val uint16) { r.sp -= val }

// DecPCBy decrements the program counter by the specified amount
func (r *registers) DecPCBy(val uint16) { r.pc -= val }

const (
	bitZ  = 1 << 7
	bitN  = 1 << 6
	bitH  = 1 << 5
	bitCY = 1 << 4
)

// FlagZ indicates whether flag Z is set or not
func (r *registers) FlagZ() bool { return r.af.bottom&(bitZ) == bitZ }

// SetFlagZ sets or clears the Z flag
func (r *registers) SetFlagZ(on bool) {
	if on {
		r.af.bottom |= bitZ
	} else {
		r.af.bottom &^= bitZ
	}
}

// FlagN indicates whether flag N is set or not
func (r *registers) FlagN() bool { return r.af.bottom&(bitN) == bitN }

// SetFlagN sets or clears the N flag
func (r *registers) SetFlagN(on bool) {
	if on {
		r.af.bottom |= bitN
	} else {
		r.af.bottom &^= bitN
	}
}

// FlagH indicates whether flag H is set or not
func (r *registers) FlagH() bool { return r.af.bottom&(bitH) == bitH }

// SetFlagH sets or clears the H flag
func (r *registers) SetFlagH(on bool) {
	if on {
		r.af.bottom |= bitH
	} else {
		r.af.bottom &^= bitH
	}
}

// FlagCY indicates whether flag CY is set or not
func (r *registers) FlagCY() bool { return r.af.bottom&(bitCY) == bitCY }

// SetFlagCY sets or clears the CY flag
func (r *registers) SetFlagCY(on bool) {
	if on {
		r.af.bottom |= bitCY
	} else {
		r.af.bottom &^= bitCY
	}
}
