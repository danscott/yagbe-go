package registers_test

import (
	"math/rand"
	"testing"
	"time"

	"gitlab.com/danscott/yagbe-go/pkg/registers"
)

func TestBytePair(t *testing.T) {
	bp := registers.BytePair{}

	rand.Seed(time.Now().UnixNano())

	tVal := byte(rand.Uint32() & 0xFF)
	bVal := byte(rand.Uint32() & 0xFF)
	expectedVal := uint16(tVal)<<8 | uint16(bVal)

	bp.SetTop(tVal)
	bp.SetBottom(bVal)

	if bp.Value() != expectedVal {
		t.Errorf("Expected value to be %#x but got %#x", expectedVal, bp.Value())
	}

	pVal := uint16(rand.Uint32() & 0xFFFF)
	expectedTop := byte(pVal >> 8)
	expectedBottom := byte(pVal & 0xFF)

	bp.SetValue(pVal)
	if bp.Top() != expectedTop && bp.Bottom() != expectedBottom {
		t.Errorf("Expected to get top and bottom values: (%#v, %#v) but got (%#v, %#v)", expectedTop, expectedBottom, bp.Top(), bp.Bottom())
	}
}

func TestBC(t *testing.T) {
	r := registers.New()

	rand.Seed(time.Now().UnixNano())

	bVal := byte(rand.Uint32() & 0xFF)
	cVal := byte(rand.Uint32() & 0xFF)
	expectedVal := uint16(bVal)<<8 | uint16(cVal)

	r.SetB(bVal)
	r.SetC(cVal)

	if r.BC() != expectedVal {
		t.Errorf("Expected value to be %#x but got %#x", expectedVal, r.BC())
	}

	bcVal := uint16(rand.Uint32() & 0xFFFF)
	expectedB := byte(bcVal >> 8)
	expectedC := byte(bcVal & 0xFF)

	r.SetBC(bcVal)
	if r.B() != expectedB && r.C() != expectedC {
		t.Errorf("Expected to get top and bottom values: (%#v, %#v) but got (%#v, %#v)", expectedB, expectedC, r.B(), r.C())
	}
}

func TestFlagFlagZ(t *testing.T) {
	r := registers.New()

	rand.Seed(time.Now().UnixNano())
	af := uint16(rand.Uint32() & 0xFFFF)
	f := byte(af & 0xFF)
	r.SetAF(af)

	expectedSet := f | 1<<7

	r.SetFlagZ()
	if r.F() != expectedSet || !r.FlagZ() {
		t.Errorf("Expected F to be %08b and FlagZ to be true but got %08b, %v", expectedSet, r.F(), r.FlagZ())
	}

	expectedClear := f &^ (1 << 7)

	r.ClearFlagZ()
	if r.F() != expectedClear || r.FlagZ() {
		t.Errorf("Expected F to be %08b and FlagZ to be false but got %08b, %v", expectedClear, r.F(), r.FlagZ())
	}
}

func TestFlagFlagN(t *testing.T) {
	r := registers.New()

	rand.Seed(time.Now().UnixNano())
	af := uint16(rand.Uint32() & 0xFFFF)
	f := byte(af & 0xFF)
	r.SetAF(af)

	expectedSet := f | 1<<6

	r.SetFlagN()
	if r.F() != expectedSet || !r.FlagN() {
		t.Errorf("Expected F to be %08b and FlagN to be true but got %08b, %v", expectedSet, r.F(), r.FlagN())
	}

	expectedClear := f &^ (1 << 6)

	r.ClearFlagN()
	if r.F() != expectedClear || r.FlagN() {
		t.Errorf("Expected F to be %08b and FlagN to be false but got %08b, %v", expectedClear, r.F(), r.FlagN())
	}
}

func TestFlagFlagH(t *testing.T) {
	r := registers.New()

	rand.Seed(time.Now().UnixNano())
	af := uint16(rand.Uint32() & 0xFFFF)
	f := byte(af & 0xFF)
	r.SetAF(af)

	expectedSet := f | 1<<5

	r.SetFlagH()
	if r.F() != expectedSet || !r.FlagH() {
		t.Errorf("Expected F to be %08b and FlagH to be true but got %08b, %v", expectedSet, r.F(), r.FlagH())
	}

	expectedClear := f &^ (1 << 5)

	r.ClearFlagH()
	if r.F() != expectedClear || r.FlagH() {
		t.Errorf("Expected F to be %08b and FlagH to be false but got %08b, %v", expectedClear, r.F(), r.FlagH())
	}
}

func TestFlagFlagCY(t *testing.T) {
	r := registers.New()

	rand.Seed(time.Now().UnixNano())
	af := uint16(rand.Uint32() & 0xFFFF)
	f := byte(af & 0xFF)
	r.SetAF(af)

	expectedSet := f | 1<<4

	r.SetFlagCY()
	if r.F() != expectedSet || !r.FlagCY() {
		t.Errorf("Expected F to be %08b and FlagCY to be true but got %08b, %v", expectedSet, r.F(), r.FlagCY())
	}

	expectedClear := f &^ (1 << 4)

	r.ClearFlagCY()
	if r.F() != expectedClear || r.FlagCY() {
		t.Errorf("Expected F to be %08b and FlagCY to be false but got %08b, %v", expectedClear, r.F(), r.FlagCY())
	}
}
